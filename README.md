This repository is a copy of the [Pyahocorasick library](https://github.com/WojciechMula/pyahocorasick), using commit 90b3079d1465b49849b128f697bbbaefc57e5122.

Run `make` to build the library using `mopsa-build` and launch the analysis of the tests.
The results are written in text files by default.

Minor changes performed to analyze the library are shown in CHANGELOG.md
