# Changes made for the CPython analysis:

## Changes made to ease the analysis
- removed serialization using pickling for now
- removed windows/cygwin header imports in favor of posix
- includes python_stubs.c for mopsa analysis
- add mopsa_assert in trie_longest to ease the analysis
- unittests.py: removed conv function
- unittests.py: removed fixup for Python 2 (we don't support it and we have issues with *args)
- utils.c removed low-level accesses to strings in pymod_get_string
- utils.c changed PyUnicode_GET_LENGTH into PyUnciode_GetLength (FIXME: undef PyUnicode_GET_LENGTH in python_stubs and other lowlevel field accessors?)
- unittests.py: l207 changed generator comprehension into list comprehesnion
- Automaton.c: remove sole occurence of PyArg_ParseTupleAndKeywords with PyArg_ParseTuple. Change propagated to unittests calls (with one test removed as not applicable anymore)
- AutomatonItemsIter.c: no need for parentheses in the Py_BuildValue l256/...
## Source code fixes
### Not explicitly caught by Mopsa when removed?
- trienode_set_next: next was `void*`, moved to `Pair*`.
- trienode_set_next: `next = (TrieNode**)` replaced by `(Pair*)`.
### Explicitly caught by Mopsa when removed
- ahocorasickmodule: PyType_Ready on automaton_items_iter_type, automaton_search_iter_long_type and automaton_search_iter_type

## ???
- trienode_new: initialization of both fields in a union?
